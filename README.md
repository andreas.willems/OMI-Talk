# OMI-Talk
## Portierung der nativen App in eine hybride mobile App

### Installation:

```bash
git clone https://gitlab.com/Athyrion/OMI-Talk.git
```
```bash
cd OMI-Talk
npm install  
bower install
ionic state reset
```
```bash
ionic build [android | ios]
ionic emulate [android | ios]
```