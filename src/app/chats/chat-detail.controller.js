(function() {
	'use strict';

	angular
		.module('app.chats')
		.controller('ChatDetailController', ChatDetailController);

	ChatDetailController.$inject = [
    '$scope',
    '$state',
    '$timeout',
    '$ionicScrollDelegate',
    'ChatsService',
    'ContactsService'
  ];

	function ChatDetailController($scope,
                                $state,
                                $timeout,
                                $ionicScrollDelegate,
                                ChatsService,
                                ContactsService) {
		var vm = this;
		vm.platform = ionic.Platform.platform();
    vm.cname = '';
    vm.jid = '';
    vm.status = '';
    vm.messageText = '';
    vm.messages = [];
    vm.goToChats = goToChats;
    vm.sendMessage = sendMessage;

    /**
     * Register event listener to initialize attributes
     * with data from current contact.
     */
    $scope.$on('$ionicView.enter', function() {
      ContactsService.getCurrentContact(function(result) {
        var contact = result;
        vm.cname = contact.cname;
        vm.jid = contact.jid;
        vm.cstatus = contact.status;
        getChatHistory();
      });
    });

    /**
     * Register event listener to react to an updated
     * message history.
     */
    $scope.$on('xmpp:messages:updated', function() {
      getChatHistory();
      $timeout(function() {
        $scope.$digest();
      }, 300);
      $ionicScrollDelegate.scrollBottom();
    });

    function getChatHistory() {
      ChatsService.getByJid(vm.jid, function(result) {
        vm.messages = result;
      });
    }

    function goToChats() {
      $state.go('^.chats', {}, {location: 'replace'});
    }

    function sendMessage() {
      if (vm.messageText !== '') {
        ChatsService.sendMessage(vm.jid, vm.cname, vm.messageText, function() {
          vm.messageText = '';
          $ionicScrollDelegate.scrollBottom();
        });
      }
    }
	}
})();