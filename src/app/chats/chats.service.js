(function() {
	'use strict';

	angular
		.module('app.chats')
		.factory('ChatsService', ChatsService);

	ChatsService.$inject = [
    '$rootScope',
    'DataStorage',
    'Xmpp'
  ];

	function ChatsService($rootScope,
                        DataStorage,
                        Xmpp) {
    var vm = this;
		vm.chats = {};

    // listen for incoming message events
    /*$rootScope.$on('xmpp:message:incoming', function(event, messageStanza) {
      addIncomingMessage(messageStanza, function() {
        $rootScope.$broadcast('xmpp:messages:updated');
      });
    });*/

    return {
      addIncomingMessage: addIncomingMessage,
      getAll: getAll,
      remove: remove,
      getByJid: get,
      sendMessage: sendMessage,
      removeAll: removeAll
    };

    /**
     * Adds a new message to the chats dictionary.
     * @param key
     * @param message the message to add
     */
		function add(key, message) {
      // store in chats dictionary
      if (vm.chats[key]) {
        vm.chats[key].push(message);
      } else {
        vm.chats[key] = [];
        vm.chats[key].push(message);
      }
      // persist dictionary
      DataStorage.storeObject('chats', vm.chats, function() {
        $rootScope.$broadcast('xmpp:messages:updated');
      });

		}

    /**
     * Processes and adds an incoming message to the chats dictionary.
     * @param messageStanza
     */
    function addIncomingMessage(messageStanza) {
      var toStore = createStorableObject(messageStanza);
      add(toStore.jid, toStore);
      cordova.plugins.notification.local.schedule({
        title: 'OMI-Talk',
        text: toStore.body,
        badge: 1
      });
    }

    /**
     * Creates an object from the incoming message stanza.
     * @param chatEntry
     * @returns {{from: string, jid: string, to: string,
     *            type: string, xmlns: string, body: string,
     *            timestamp: number}}
     */
    function createStorableObject(chatEntry) {
      var x2js = new X2JS();
      var asJson = x2js.xml2json(chatEntry);
      var jid = Strophe.getBareJidFromJid(asJson._from).toLowerCase();
      return {
        'from': asJson._from,
        'jid': jid,
        'cname': asJson._name,
        'to': asJson._to,
        'type': asJson._type,
        'xmlns': asJson._xmlns,
        'body': asJson.body,
        'timestamp': Date.now()
      };
    }

    /**
     * Gets the complete chats dictionary.
     */
		function getAll(cb) {
      // test if chats dictionary is empty
      if (Object.keys(vm.chats).length === 0) {
        DataStorage.getObject('chats', function(result) {
          if (result) {
            vm.chats = result;
          }
        });
      }
      if (cb) {
        cb(vm.chats);
      }
		}

    /**
     * Removes the entries with the given key
     * from the chats dictionary.
     * @param jid the entry to remove
     * @param cb a callback function
     */
		function remove(jid, cb) {
      if (vm.chats[jid]) {
        delete vm.chats[jid];
        // persist dictionary
        DataStorage.storeObject('chats', vm.chats);
        cb();
      }
		}

    /**
     * Gets the entry with the given jid.
     * @param jid the needed entry's jid.
     * @param cb
     * @returns {*}
     */
		function get(jid, cb) {
			if (vm.chats[jid]) {
        return cb(vm.chats[jid]);
      }
      return cb(null);
		}

    /**
     * Sends a message to the recipient using the xmpp messages service.
     * @param recipient the message's recipient
     * @param cname the recipient's name
     * @param message the content to send
     * @param callback function called after sending
     */
    function sendMessage(recipient, cname, message, callback) {
      Xmpp.sendMessage(recipient, message, function(connection) {
        var obj = {
          'from': connection.authzid,
          'cname': cname,
          'jid': connection.jid,
          'to': recipient,
          'type': 'chat',
          'xmlns': 'jabber:client',
          'body': message,
          'timestamp': Date.now()
        };
        add(recipient, obj);
        callback();
      });
    }

    function removeAll() {
      DataStorage.removeByKey('chats');
      vm.chats = {};
    }
	}
})();