(function() {
	'use strict';

	angular
		.module('app.chats')
		.controller('ChatsController', ChatsController);

	ChatsController.$inject = [
    '$scope',
    '$timeout',
    'ChatsService',
    'ContactsService'
  ];

	function ChatsController($scope,
                           $timeout,
                           ChatsService,
                           ContactsService) {
		var vm = this;
		vm.chats = [];
		vm.remove = remove;
    vm.setCurrentContact = setCurrentContact;

    /**
     * Register event listener to react to an updated
     * message history.
     */
    $scope.$on('xmpp:messages:updated', function() {
      getChats();
      $timeout(function() {
        $scope.$digest();
      }, 300);
    });

    $scope.$on('$ionicView.enter', function() {
      getChats();
      //$scope.$digest();
    });

		activate();

		////////////////

		function activate() {
			getChats();
		}

    function getChats() {
      // get stored contacts dictionary
      ContactsService.getContactsDictionary(function(contacts) {
        // get stored chats
        ChatsService.getAll(function(result) {
          // reset local chat array
          vm.chats = [];
          // get keys from stored chats
          var keys = Object.keys(result);
          // use temporary array to not mess with scope's array
          var tmpArr = [];
          // iterate over chats and create list to display
          for (var i = 0; i < keys.length; i++) {
            // get messages for key
            var messages = result[keys[i]];
            // get last message item
            var last = messages[messages.length - 1];
            // get the associated name from the contacts dictionary
            var name = contacts[keys[i]];
            // create and push list entry
            tmpArr.push({
              name: name,
              jid: keys[i],
              recentDate: new Date(last.timestamp),
              recentBody: last.body
            });
          }
          vm.chats = tmpArr;

        });
      });
    }

		function remove(jid) {
			ChatsService.remove(jid, function() {
        activate();
      });
		}

    function setCurrentContact(contact) {
      ContactsService.setCurrentContact({
        jid: contact.jid,
        cname: contact.name,
        status: ''
      });
    }
	}
})();