(function() {
	'use strict';
	
	angular
	  .module('app')
	  .controller('LayoutController', LayoutController);
	  
	  LayoutController.$inject = [
			'$rootScope',
			'ChatsService'
		];
	  
	  function LayoutController($rootScope,
															ChatsService) {
		  var vm = this;
		  vm.platform = ionic.Platform.platform();

			// listen for incoming message events
			// defined here, because this is the first controller that gets initialized
			$rootScope.$on('xmpp:message:incoming', function(event, messageStanza) {
				ChatsService.addIncomingMessage(messageStanza, function() {
					//$rootScope.$broadcast('xmpp:messages:updated');
				});
			});

			activate();

      function activate() {
        // init chat histories here, because this is the first controller
        // that gets activated
        ChatsService.getAll(function(chats) {
          //console.log(chats);
        });
      }
	  }
})();