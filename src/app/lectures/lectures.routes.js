(function() {
	'use strict';

	angular
		.module('app.lectures')
		.config(routes);

	routes.$inject = ['$stateProvider'];

	function routes($stateProvider) {
		$stateProvider

		.state('tab.lectures', {
			url: '/lectures',
			views: {
				'tab-lectures': {
					templateUrl: 'app/lectures/lectures.html',
					controller: 'LecturesController as vm'
				}
			}
		});
	}
})();
