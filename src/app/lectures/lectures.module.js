(function() {
	'use strict';

	angular
		.module('app.lectures', [
			'ionic',
			'ngCordova'
		]);
})();
