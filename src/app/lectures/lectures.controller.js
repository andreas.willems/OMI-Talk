(function() {
	'use strict';

	angular
		.module('app.lectures')
		.controller('LecturesController', LecturesController);

	LecturesController.$inject = [
    '$scope',
    'LecturesService'
  ];

	function LecturesController($scope, LecturesService) {
		var vm = this;
    vm.lectures = [];
    vm.fetchLectures = fetchLectures;

    $scope.$on('xmpp:lectures:update', function() {
      vm.fetchLectures();
    });

		activate();

		////////////////

		function activate() {
      vm.fetchLectures();
    }

    function fetchLectures() {
      LecturesService.getAll(function(result) {
        if (result.length > 0) {
          vm.lectures = result;
        }
        $scope.$broadcast('scroll.refreshComplete');
      });
    }
	}
})();
