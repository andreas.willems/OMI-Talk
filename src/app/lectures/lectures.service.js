(function() {
  'use strict';

  angular
    .module('app.lectures')
    .factory('LecturesService', LecturesService);

  LecturesService.$inject = [
    '$http',
    '$q',
    '$timeout',
    '$rootScope',
    'Xmpp'
  ];

  function LecturesService($http, $q, $timeout, $rootScope, Xmpp) {
    //var serverUrl = 'http://192.168.1.50:3000/omitalk/service';
    var serverUrl = 'http://lyra.et-inf.fho-emden.de:14416/omitalk/service';

    return {
      getAll: getAll,
      registerLecture: registerLecture
    };

    function getAll(cb) {
      Xmpp.getUserJid(function(jid) {
        var req = {
          method: 'GET',
          headers : {},
          url: serverUrl + '/lectures?studentName=' + jid
        };
        $http(req)
          .then(onSuccess, onError);
      });

      function onSuccess(response) {
        console.log('success:');
        console.log(response);
        return cb(response.data);
      }

      function onError(response) {
        console.log('error:');
        console.log(response);
        cb(response);
      }
    }

    function registerLecture(lectureData, cb) {
      Xmpp.getUserJid(function(jid) {
        lectureData['studentName'] = jid;
        var req = {
          method: 'POST',
          url: serverUrl + '/lecture/register',
          headers : {},
          data: lectureData
        };
        $http(req)
          .then(onSuccess, onError);
      });

      function onSuccess(response) {
        console.log('success:');
        console.log(response);
        $rootScope.$broadcast('xmpp:lectures:update');
        cb();
      }

      function onError(response) {
        console.log('error:');
        console.log(response);
        cb(response);
      }
    }
  }
})();
