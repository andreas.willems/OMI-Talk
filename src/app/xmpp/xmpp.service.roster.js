(function() {
  'use strict';

  angular
    .module('xmpp')
    .factory('XmppRoster', XmppRoster);

  XmppRoster.$inject = ['$log'];

  /**
   * Constructor.
   * @returns {{getRoster: getRoster, delRoster: delRoster}}
   * @constructor
   */
  function XmppRoster() {
    var connection;
    var roster;
    return {
      getRoster: getRoster,
      getLocalRoster: getLocalRoster,
      delRoster: delRoster
    };

    /**
     * Fetches the user's roster from the server.
     * @param conn the connection to the server
     * @param cb the function to call after the roster was received
     */
    function getRoster(conn, cb) {
      connection = conn;
      //var iq = new Strophe.Builder('iq', {type: 'get'})
      var iq = $iq({type: 'get'})
        .c('query', {xmlns: 'jabber:iq:roster'});

      conn.sendIQ(iq, function onRoster(response) {
        handleRoster(response, cb);
      });
    }

    /**
     * Returns the local instance of the 'roster' field.
     * @returns an array
     */
    function getLocalRoster() {
      return roster;
    }

    /**
     * Converts the fetched data from xml to JSON.
     * @param response the server's response
     * @param cb the function to call after the roster was converted
     */
    function handleRoster(response, cb) {
      var x2js = new X2JS();
      var xmlToJson = x2js.xml2json(response);
      //console.log(xmlToJson);
      var rosterAsArray = xmlToJson.query.item;
      if (rosterAsArray.length > 1) {
        rosterAsArray.sort(compare);
      } else {
        rosterAsArray = [rosterAsArray];
      }
      roster = rosterAsArray;
      initPresence(roster, function() {
        cb(roster);
      });
    }

    /**
     * Deletes the roster.
     */
    function delRoster() {
      roster = [];
    }

    /**
     * Sets the status 'offline' to all items in the roster.
     * @param roster the user's roster
     * @param cb callback method
     * @returns {*}
     */
    function initPresence(roster, cb) {
      if (roster.length) {
        var i = 0;
        var length = roster.length;
        for (i; i < length; i++) {
          roster[i]._status = 'unavailable';
        }
      } else {
        roster._status = 'unavailable';
      }
      cb();
    }

    /**
     * Compares the given parameter's by attribute.
     * @param a
     * @param b
     * @returns {number}
     */
    function compare(a, b) {
      if (a._name < b._name) {
        return -1;
      }
      if (a._name > b._name) {
        return 1;
      }
      return 0;
    }
  }
})();
