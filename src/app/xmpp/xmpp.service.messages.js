(function() {
  'use strict';

  angular
    .module('xmpp')
    .factory('XmppMessages', XmppMessages);

  XmppMessages.$inject = [];

  /**
   * Constructor.
   * @returns {{sendMessage: sendMessage}}
   * @constructor
   */
  function XmppMessages() {
    var connection;
    return {
      sendMessage: sendMessage
    };

    /**
     * Sends a message to the xmpp server with the given
     * parameters.
     * @param conn the connection to use
     * @param recipient the message's recipient
     * @param message the content to send
     * @param callback the function to be called after sending the message
     */
    function sendMessage(conn, recipient, message, callback) {
      connection = conn;
      conn.send($msg({
        'to': recipient,
        'type': 'chat'
      }).c('body').t(message));
      callback(connection);
    }

    /**
     * Compares the given parameter's by attribute.
     * @param a
     * @param b
     * @returns {number}
     */
    function compare(a, b) {
      if (a._name < b._name) {
        return -1;
      }
      if (a._name > b._name) {
        return 1;
      }
      return 0;
    }
  }
})();
