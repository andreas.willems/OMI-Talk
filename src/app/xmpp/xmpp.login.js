(function() {
  'use strict';

  angular
    .module('xmpp')
    .factory('XmppLogin', XmppLogin);

  XmppLogin.$inject = [
    '$rootScope',
    '$timeout',
    'DataStorage',
    'XmppRoster'
  ];

  /**
   * Creates a connection to the XMPP server and authenticates
   * the user with provided credentials. After a successful
   * login, some handlers are attached to the connection
   * that react to different events.
   * @param $timeout
   * @param $rootScope
   * @param DataStorage
   * @param XmppRoster
   * @returns {{login: login}}
   * @constructor
   */
  function XmppLogin($rootScope, $timeout, DataStorage, XmppRoster) {
    //var bosh = 'http://192.168.1.50:7070/http-bind/';
    var bosh = 'http://lyra.et-inf.fho-emden.de:14414/http-bind/';
    var conn;
    return {
      login: login,
      getConnection: getConnection
    };

    /**
     * Connect and authenticate.
     * @param jid
     * @param password
     * @param onSuccess
     * @param onError
     * @param cb
     */
    function login(jid, password, onSuccess, onError, cb) {
      conn = new Strophe.Connection(bosh, {'keepalive': false});
      conn.rawInput = rawInput;
      conn.rawOutput = rawOutput;

      // start a timer to check, if the server is available.
      // the returned promise will be cancelled in case of a
      // connection
      var timeoutPromise = $timeout(function() {
        onError('Server nicht verfügbar.');
      }, 10000);
      conn.connect(jid, password, function(status) {
        if (status === Strophe.Status.CONNECTED) {
          $timeout.cancel(timeoutPromise);

          //console.log(conn);

          DataStorage.storeObject('credentials', {
            'authzid': conn.authzid,
            'pass': conn.pass
          });
          registerHandlers(function() {
            onSuccess();
            // "return" connection object
            return cb(conn);
          });
        } else {
          if (status === Strophe.Status.CONNFAIL) {
            $timeout.cancel(timeoutPromise);
            console.log(status);
            onError('Verbindung nicht möglich.');
          }

          if (status === Strophe.Status.AUTHFAIL) {
            $timeout.cancel(timeoutPromise);
            console.log(status);
            onError('Authentifizierung nicht möglich.');
          }

          if (status === Strophe.Status.ERROR) {
            $timeout.cancel(timeoutPromise);
            console.log(status);
            onError('Ein Fehler ist aufgetreten.');
          }
        }
      });
    }

    /**
     * Starts registering of xmpp handlers.
     */
    function registerHandlers(cb) {
      addPingHandling();
      addPresenceHandling();
      addMessageHandling();
      cb();
    }

    /**
     * Adds handling for pings from the server.
     */
    function addPingHandling() {
      conn.ping.addPingHandler(onPing);
    }

    /**
     * Handles ping events by responding with a pong.
     */
    function onPing(ping) {
      console.log(conn.ping.pong);
      conn.ping.pong(ping);
      return true;
    }

    /**
     * Adds handling of XMPP presence stanzas.
     */
    function addPresenceHandling() {
      conn.addHandler(onPresence, null, 'presence');
      conn.send($pres());
      return true;
    }

    /**
     * Handles presence events.
     * @param presence the presence stanza sent from the server.
     * @returns {boolean}
     */
    function onPresence(presence) {
      var x2js = new X2JS();
      var xmlToJson = x2js.xml2json(presence);
      var type = xmlToJson._type;
      var from = xmlToJson._from;
      var jid = Strophe.getBareJidFromJid(from).toLowerCase();
      var contact = getContactByJid(jid);
      if (contact) {
        if (type) {
          contact._status = type;
        } else {
          contact._status = 'available';
        }
      }
      $rootScope.$broadcast('xmpp:presence:update');
      return true;
    }

    /**
     * Gets a contact from the roster by it's jid.
     * @param jid the wanted contact's jid
     * @returns {*}
     */
    function getContactByJid(jid) {
      var roster = XmppRoster.getLocalRoster();
      if (roster) {
        var i = 0;
        var length = roster.length;
        var result;
        for (i; i < length; i++) {
          if (roster[i]._jid === jid) {
            result = roster[i];
            break;
          }
        }
        return result;
      }
      return null;
    }

    /**
     * Adds handling of XMPP message stanzas.
     */
    function addMessageHandling() {
      conn.addHandler(onMessage, null, 'message', 'chat');
      return true;
    }

    /**
     * Handles incoming messages by passing them over
     * to the ChatsService.
     * @param messageStanza the incoming message
     */
    function onMessage(messageStanza) {
      $rootScope.$broadcast('xmpp:message:incoming', messageStanza);
      return true;
    }

    function getConnection(cb) {
      cb(conn);
    }

    function rawInput(data) {
      //console.log('RECV: ' + data);
    }

    function rawOutput(data) {
      //console.log('SENT: ' + data);
    }
  }
})();
