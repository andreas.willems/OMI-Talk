(function() {
  'use strict';

  angular
    .module('xmpp')
    .factory('Xmpp', Xmpp);

  Xmpp.$inject = [
    'DataStorage',
    'XmppLogin',
    'XmppRoster',
    'XmppMessages',
    'XmppLectures'
  ];

  /**
   * Constructor.
   * @param DataStorage
   * @param XmppLogin
   * @param XmppRoster
   * @param XmppMessages
   * @param XmppLectures
   * @returns {{login: login, logout: logout, getRoster: getRoster, sendMessage: sendMessage}}
   * @constructor
   */
  function Xmpp(DataStorage,
                XmppLogin,
                XmppRoster,
                XmppMessages,
                XmppLectures) {
    var vm = this;
    vm.conn = null;
    vm.jid = '';
    return {
      login: login,
      logout: logout,
      getUserJid: getUserJid,
      getRoster: getRoster,
      sendMessage: sendMessage,
      registerLecture: registerLecture
    };

    function login(jid, password, onSuccess, onError) {
      XmppLogin.login(jid, password, onSuccess, onError, function(connection) {
        if (connection) {
          vm.conn = connection;
          vm.jid = connection.authzid;
        }
      });
    }

    function logout(cb) {
      if(vm.conn) {
        XmppRoster.delRoster();
        DataStorage.removeByKey('credentials');
        vm.conn.disconnect('Wanted logout');
        cordova.plugins.backgroundMode.disable();
        cb();
      }
    }

    function getUserJid(cb) {
      cb(vm.jid);
    }

    function getRoster(cb) {
      XmppRoster.getRoster(vm.conn, cb);
    }

    function sendMessage(recipient, message, cb) {
      XmppMessages.sendMessage(vm.conn, recipient, message, cb);
    }

    function registerLecture(studentName, subject, lecturerName, lectureNr,
                             callback) {
      XmppLectures.registerLecture(
        studentName, subject, lecturerName, lectureNr, callback
      );
    }
  }
})();
