(function() {
  'use strict';

  angular
    .module('xmpp')
    .factory('XmppLectures', XmppLectures);

  XmppLectures.$inject = ['XmppLogin'];


  function XmppLectures(XmppLogin) {

    var namespace = 'http://omi.talk.com/lecture';
    var elementName = 'lecture';

    return {
      getLectures: getLectures,
      registerLecture: registerLecture
    };

    function getLectures() {

    }

    function registerLecture(studentName, subject, lecturerName, lectureNr,
                             callback) {
      XmppLogin.getConnection(function(conn) {
        if (conn) {
          var msg = $msg({'type': 'lecture'})
            .c('lecture', {
              'studentName': studentName,
              'subjectName': subject,
              'lecturerName': lecturerName,
              'lectureNr': lectureNr
            });
          conn.send(msg);
          callback();
        }
      });
    }
  }
})();
