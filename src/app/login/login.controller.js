(function() {
	'use strict';

	angular
		.module('login')
		.controller('LoginController', LoginController);

	LoginController.$inject = [
    '$scope',
    '$state',
    '$cordovaDialogs',
    '$ionicHistory',
    '$ionicLoading',
    'DataStorage',
    'Xmpp'];

	function LoginController($scope,
                           $state,
                           $cordovaDialogs,
                           $ionicHistory,
                           $ionicLoading,
                           DataStorage,
                           Xmpp) {
		var vm = this;
		vm.username = '';
    vm.password = '';
		vm.login = login;
    vm.showAlert = showAlert;
    vm.showLoading = showLoading;
    vm.hideLoading = hideLoading;

		activate();

		function activate() {
      // check if credentials already exist
      DataStorage.getObject('credentials', function(result) {
        // if exists, perform automated login
        if (result) {
          vm.username = result.authzid;
          vm.password = result.pass;
          vm.login(vm.username, vm.password, onLogin, onError);
        }
      });
		}

    $scope.$on('$ionicView.enter', function() {
      $ionicHistory.clearCache();
    });

    function showAlert(message) {
      $cordovaDialogs.alert(
        message,
        'Validieren fehlgeschlagen',
        'OK'
      );
    }

    function showLoading() {
      $ionicLoading.show();
    }

    function hideLoading() {
      $ionicLoading.hide();
    }
		
		function login() {
      if (validateUsername(vm.username) && validatePassword(vm.password)) {
        vm.showLoading();
				Xmpp.login(vm.username, vm.password, onLogin, onError);
      } else {
        vm.showAlert('Benutzername und/oder Code fehlen');
      }
		}

    function onLogin() {
      vm.hideLoading();
      $state.go('tab.contacts');
    }

    function onError(message) {
      vm.hideLoading();
      vm.showAlert(message);
    }

    function validateUsername(username) {
      return username !== '';
    }

    function validatePassword(password) {
      return password !== '';
    }
	}
})();