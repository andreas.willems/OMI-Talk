(function() {
	'use strict';

	angular
		.module('app', [
			/* Shared modules */
			'ionic',
			'app.core',
			'pascalprecht.translate',

			/* Feature areas */
			'aw.actionsheet',
			'datastorage',
			'login',
			'xmpp',
			'app.contacts',
			'app.chats',
			'app.lectures',
			'app.settings'
		]);
})();
