(function() {
    'use strict';

	angular
		.module('app')
		.run(runBlock);

	runBlock.$inject = [];

	function runBlock() {

    document.addEventListener('deviceready', function() {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        cordova.plugins.Keyboard.disableScroll(true);
        cordova.plugins.notification.local.hasPermission(function(granted) {
          if (!granted) {
            cordova.plugins.notification.local.registerPermission(function (granted) {});
          }
        });
        cordova.plugins.notification.badge.hasPermission(function (granted) {
          if (!granted) {
            cordova.plugins.notification.badge.registerPermission(function (granted) {});
          }
          cordova.plugins.notification.badge.configure({autoClear: true});
        });
        cordova.plugins.backgroundMode.enable();
        cordova.plugins.backgroundMode.setDefaults({
          title: 'OMI-Talk',
          text: '...wartet, dass jemand schreibt...',
          silent: true
        });
      }

      if (window.StatusBar) {
        StatusBar.styleLightContent();
        //StatusBar.styleDefault();
      }

    }, false);
	}
})();
