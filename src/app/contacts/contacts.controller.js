(function() {
	'use strict';

	angular
		.module('app.contacts')
		.controller('ContactsController', ContactsController);

	ContactsController.$inject = [
    '$scope',
    'ContactsService',
		'Xmpp'
	];

	function ContactsController($scope,
                              ContactsService,
                              Xmpp) {
		var vm = this;
		vm.roster = [];
    vm.setCurrentContact = setCurrentContact;

    $scope.$on('xmpp:presence:update', function() {
      $scope.$digest();
    });

		activate();

		function activate() {
      // init contact list
      getRoster();
		}

    function getRoster() {
      Xmpp.getRoster(function(roster) {
        vm.roster = roster;
        $scope.$digest();
        ContactsService.storeContacts(roster);
      });
    }

    function setCurrentContact(contact) {
      console.log(contact);
      ContactsService.setCurrentContact({
        jid: contact._jid,
        cname: contact._name,
        status: contact._status
      });
    }
	}
})();