(function() {
  'use strict';

  angular
    .module('app.chats')
    .factory('ContactsService', ContactsService);

  ContactsService.$inject = [
    'DataStorage'
  ];

  function ContactsService(DataStorage) {
    var currentContact = null;
    return {
      getCurrentContact: getCurrentContact,
      setCurrentContact: setCurrentContact,
      storeContacts: storeContactsDictionary,
      getContactsDictionary: getContactsDictionary
    };

    function getCurrentContact(cb) {
      cb(currentContact);
    }

    function setCurrentContact(contact) {
      currentContact = contact;
    }

    function storeContactsDictionary(roster) {
      createContactDictionary(roster, function(contacts) {
        DataStorage.storeObject('contacts', contacts);
      });
    }

    function createContactDictionary(roster, cb) {
      var contacts = {};
      for (var i = 0; i < roster.length; i++) {
        var jid = roster[i]._jid;
        var name = roster[i]._name;
        contacts[jid] = name;
      }
      return cb(contacts);
    }

    function getContactsDictionary(cb) {
      DataStorage.getObject('contacts', function(contacts) {
        if (!contacts) {
          return cb(null);
        }
        return cb(contacts);
      });
    }
  }
})();