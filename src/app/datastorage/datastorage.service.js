(function() {
  'use strict';

  angular
    .module('datastorage')
    .factory('DataStorage', DataStorage);

  DataStorage.$inject = ['$window'];

  function DataStorage($window) {
    return {
      storeObject: storeObject,
      getObject: getObject,
      removeByKey: removeByKey
    };

    function storeObject(key, obj, cb) {
      $window.localStorage[key] = JSON.stringify(obj);
      if (cb) {
        cb();
      }
    }

    function getObject(key, cb) {
      if ($window.localStorage[key]) {
        var obj = JSON.parse($window.localStorage[key]);
        return cb(obj);
      }
      return cb(null);
    }

    function removeByKey(key) {
      if ($window.localStorage[key]) {
        $window.localStorage.removeItem(key);
      }
    }
  }
})();
