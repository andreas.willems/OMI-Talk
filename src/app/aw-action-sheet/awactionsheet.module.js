(function() {
  'use strict';

  angular
    .module('aw.actionsheet', [
      'ionic',
      'ngCordova'
  ]);
})();