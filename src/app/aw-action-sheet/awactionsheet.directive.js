(function() {
  'use strict';

  angular
    .module('aw.actionsheet')
    .directive('awActionSheet', AwActionSheet);

  AwActionSheet.$inject = [
    '$cordovaActionSheet',
    '$cordovaBarcodeScanner',
    '$cordovaDialogs',
    '$ionicLoading',
    '$state',
    '$timeout',
    'ChatsService',
    'LecturesService',
    'Xmpp'
  ];

  function AwActionSheet($cordovaActionSheet,
                         $cordovaBarcodeScanner,
                         $cordovaDialogs,
                         $ionicLoading,
                         $state,
                         $timeout,
                         ChatsService,
                         LecturesService,
                         Xmpp) {
    return {
      templateUrl: 'app/aw-action-sheet/awactionsheet.template.html',
      link: link
    };

    function link(scope, element, attrs) {
      scope.platform = ionic.Platform.platform();
      scope.showActionSheet = showActionSheet;
    }

    function showActionSheet() {
      var options = {
        title: 'Aktion wählen',
        buttonLabels: ['QR-Code scannen', 'Ausloggen', 'Delete chat history'],
        addCancelButtonWithLabel: 'Abbrechen',
        androidEnableCancelButton: false,
        winphoneEnableCancelButton: false
      };
      document.addEventListener('deviceready', function () {
        $cordovaActionSheet
          .show(options)
          .then(function(index) {
            // button index starts at 1 !!!
            if (index === 1) {
              scanBarcode();
            } else if (index === 2) {
              logout();
            } else if (index === 3) {
              deleteChatHistory();
            }
            return true;
          });
      });
    }

    function deleteChatHistory() {
      ChatsService.removeAll();
    }

    function scanBarcode() {
      $cordovaBarcodeScanner
        .scan()
        .then(function onSuccess(result) {
          var lectureData = parseScanResult(result);
          var dialogTexts = createScanDialogMessage(lectureData);
          $timeout(function() {
            $cordovaDialogs
              .confirm(dialogTexts[0], dialogTexts[1], ['Ja', 'Nein'])
              .then(function(btnIndex) {
                handleConfirmResult(btnIndex, lectureData);
              });
          }, 500);
        }, function onError(error) {
          // show error dialog
          $cordovaDialogs
            .alert('Scannen fehlgeschlagen', 'Fehler', 'Ok');
        });
    }

    function handleConfirmResult(btnIndex, lectureData) {
      // no button = 0, Ja = 1, Nein = 2
      if (btnIndex === 1) {
        // add to lectures service
        LecturesService.registerLecture(lectureData, function() {
          // do some popup or so
          $ionicLoading.show({
            template: 'Registrierung erfolgreich',
            duration: '2500'
          });
        });
      }
    }

    function parseScanResult(scanResult) {
      // parse result and create an object
      var tmp = JSON.parse(scanResult.text);
      return {
        'subjectName': tmp['subjectName'],
        'lecturerName': tmp['lecturerName'],
        'lectureNr': tmp['lectureNr']
      };
    }

    function createScanDialogMessage(lectureData) {
      var title = 'Bei Präsenz anmelden?';
      var message = 'Kurs: ' + lectureData.subjectName + '\n';
      message += 'Nummer: ' + lectureData.lectureNr;
      return [message, title];
    }

    function logout() {
      Xmpp.logout(function() {
        $state.go('login', {}, {location: 'replace'});
      });
    }
  }
})();