(function() {
	'use strict';

	angular
		.module('app.settings')
		.config(routes);

	routes.$inject = ['$stateProvider'];

	function routes($stateProvider) {
		$stateProvider

		.state('tab.settings', {
			url: '/settings',
			views: {
				'tab-settings': {
					templateUrl: 'app/settings/settings.html',
					controller: 'SettingsController as vm'
				}
			}
		});
	}
})();
