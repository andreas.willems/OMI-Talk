(function() {
	'use strict';

	angular
		.module('app')
		.config(configure);

		configure.$inject = ['$translateProvider'];

	function configure ($translateProvider) {
	  // Add your configuration here
	  $translateProvider.translations('de', {
		  TAB_CONTACTS:  'Kontakte',
		  TAB_CHATS: 'Nachrichten',
		  TAB_LECTURES: 'Präsenzen',
		  TAB_SETTINGS: 'Einstellungen'
		});
		
	  $translateProvider.translations('en', {
		  TAB_CONTACTS:  'Contacts',
		  TAB_CHATS: 'Chats',
		  TAB_LECTURES: 'Lectures',
		  TAB_SETTINGS: 'Settings'
		});

  	// Don't forget: the standard language
    $translateProvider.preferredLanguage('de');
	  
	  // Enable escaping of HTML
  	$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
	}

})();
